FROM mcr.microsoft.com/dotnet/core/runtime:3.1

COPY Src/Presentation/Canine.Net.Client/bin/Release/netcoreapp3.1/publish/ /App

WORKDIR /App

ENTRYPOINT ["dotnet", "Canine.Net.User.PostgreSQL.dll"]