namespace Canine.Net.User.PostgreSQL.Client.Resources
{
    using Canine.Net.Infrastructure.Command;
    using Canine.Net.Infrastructure.Console;
    using Monosoft.User.Application.Common.Services;
    using Monosoft.User.Domain.Interfaces;
    using Command = Monosoft.User.Application.Resources.Token.Commands;

    internal class TokenResource : BaseResource
    {
        public TokenResource(
            IUserRepository userRepo,
            IUserLoginLogRepository logRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            ITokenRepository tokenRepo,
            IEventService eventService) : base("Token")
        {
            this.AddCommand(new Command.Login.Command(userRepo, logRepo, uinugRepo, ugRepo, tokenRepo, eventService), OperationType.Execute, "Login", "Login to canine");
            this.AddCommand(new Command.Logout.Command(tokenRepo, eventService), OperationType.Execute, "Logout", "Logout of canine");
            this.AddCommand(new Command.Refresh.Command(tokenRepo, eventService), OperationType.Execute, "Refresh", "Refresh token time");
            this.AddCommand(new Command.Verify.Command(tokenRepo), OperationType.Execute, "Verify", "Return used token, with all claims");
        }
    }
}
