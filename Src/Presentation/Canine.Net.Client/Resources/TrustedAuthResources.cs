namespace Canine.Net.User.PostgreSQL.Client.Resources
{
    using Canine.Net.Infrastructure.Command;
    using Canine.Net.Infrastructure.Console;
    using Monosoft.User.Domain.Interfaces;
    using Command = Monosoft.User.Application.Resources.TrustedAuth.Commands;

    internal class TrustedAuthResource : BaseResource
    {
        public TrustedAuthResource(
            ITrustedAuthRepository taRepo,
            ITokenRepository tokenRepo) : base("TrustedAuth")
        {
            this.AddCommand(new Command.Login.Command(taRepo, tokenRepo), OperationType.Execute, "Login", "Login to external canine");
            this.AddCommand(new Command.Register.Command(taRepo), OperationType.Execute, "Register", "Register external canine server", Claim.IsOperator);
            this.AddCommand(new Command.Unregister.Command(taRepo), OperationType.Execute, "Unregister", "Remove external canine server", Claim.IsOperator);
            this.AddCommand(new Command.Verify.Command(taRepo, tokenRepo), OperationType.Execute, "Verify", "Verify token on external canine", Claim.IsConsumer);
        }
    }
}
