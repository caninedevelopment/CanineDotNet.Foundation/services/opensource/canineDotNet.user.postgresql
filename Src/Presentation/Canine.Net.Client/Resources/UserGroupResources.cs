namespace Canine.Net.User.PostgreSQL.Client.Resources
{
    using Canine.Net.Infrastructure.Command;
    using Canine.Net.Infrastructure.Console;
    using Monosoft.User.Application.Common.Services;
    using Monosoft.User.Domain.Interfaces;
    using Command = Monosoft.User.Application.Resources.UserGroup.Commands;

    internal class UserGroupResource : BaseResource
    {
        public UserGroupResource(
            IUserGroupRepository ugRepo,
            IUserInUserGroupRepository uinugRepo,
            IEventService eventService) : base("UserGroup")
        {
            this.AddCommand(new Command.GetAll.Command(ugRepo), OperationType.Get, "GetAll", "Get all usergroups", Claim.IsOperator);
            this.AddCommand(new Command.Update.Command(ugRepo, uinugRepo, eventService), OperationType.Update, "Update", "Update usergroup", Claim.IsOperator);
            this.AddCommand(new Command.Insert.Command(ugRepo), OperationType.Insert, "Insert", "Insert usergroup", Claim.IsOperator);
            this.AddCommand(new Command.Delete.Command(ugRepo, uinugRepo, eventService), OperationType.Delete, "Delete", "Delete usergroup", Claim.IsOperator);
            this.AddCommand(new Command.AddUserToUserGroup.Command(uinugRepo, eventService), OperationType.Execute, "AddUserToUsergroup", "Insert user into usergroup", Claim.IsOperator);
            this.AddCommand(new Command.RemoveUserFromUserGroup.Command(uinugRepo, eventService), OperationType.Execute, "RemoveUserFromUsergroup", "Remove user from usergroup", Claim.IsOperator);
        }
    }
}
