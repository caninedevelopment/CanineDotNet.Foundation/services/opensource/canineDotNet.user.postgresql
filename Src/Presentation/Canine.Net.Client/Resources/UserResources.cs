namespace Canine.Net.User.PostgreSQL.Client.Resources
{
    using Canine.Net.Infrastructure.Command;
    using Canine.Net.Infrastructure.Console;
    using Monosoft.User.Domain.Interfaces;
    using Command = Monosoft.User.Application.Resources.User.Commands;

    internal class UserResource : BaseResource
    {
        public UserResource(
            IUserRepository uRepo,
            IUserInUserGroupRepository uinugRepo,
            IUserGroupRepository ugRepo,
            ISettingsRepository setRepo) : base("User")
        {
            this.AddCommand(new Command.Get.Command(uRepo, uinugRepo, ugRepo), OperationType.Get, "Get", "Get user by id", Claim.IsOperator);
            this.AddCommand(new Command.GetAll.Command(uRepo, uinugRepo, ugRepo), OperationType.Get, "GetAll", "Get all users", Claim.IsOperator);
            this.AddCommand(new Command.Update.Command(uRepo), OperationType.Update, "Update", "Update user via user id", Claim.IsOperator);
            this.AddCommand(new Command.Insert.Command(uRepo), OperationType.Insert, "Insert", "Insert new user", Claim.IsOperator);
            this.AddCommand(new Command.Delete.Command(uRepo), OperationType.Delete, "Delete", "Delete user via id", Claim.IsOperator);
            this.AddCommand(new Command.AnonymCreate.Command(setRepo, uRepo, uinugRepo), OperationType.Execute, "AnonymCreate", "Create new user and send mail to email", Claim.IsOperator);
            this.AddCommand(new Command.ChangePassword.Command(uRepo), OperationType.Execute, "ChangePasword", "Change your own password", Claim.IsConsumer);
            this.AddCommand(new Command.ForgotPassword.Command(uRepo, setRepo), OperationType.Execute, "ForgotPassword", "Reset password, send mail to user with reset token");
            this.AddCommand(new Command.GetMyData.Command(uRepo, uinugRepo, ugRepo), OperationType.Execute, "GetMyData", "Get own user data", Claim.IsConsumer);
            this.AddCommand(new Command.ResetPassword.Command(uRepo), OperationType.Execute, "ResetPassword", "Reset password, and return reset token");
            this.AddCommand(new Command.ResetPasswordByToken.Command(uRepo), OperationType.Execute, "ResetPasswordByToken", "Use reset token to create new password");
            this.AddCommand(new Command.Setup.Command(setRepo), OperationType.Execute, "Setup", "Setup user service, for SMTP, what group annonyme user should have..", Claim.IsAdmininistrator);
            this.AddCommand(new Command.UpdateMyData.Command(uRepo), OperationType.Execute, "UpdateMyData", "Update own user data", Claim.IsConsumer);
        }
    }
}
