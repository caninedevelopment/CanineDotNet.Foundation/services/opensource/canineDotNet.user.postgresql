namespace Canine.Net.User.PostgreSQL.Client
{
    using Canine.Net.Infrastructure.Console;
    using Canine.Net.User.PostgreSQL.Client.Resources;
    using Microsoft.Extensions.DependencyInjection;

    public static class DependencyInjection
    {
        public static IServiceCollection AddResources(this IServiceCollection services)
        {
            services.AddTransient<IResource, TokenResource>();
            services.AddTransient<IResource, TrustedAuthResource>();
            services.AddTransient<IResource, UserResource>();
            services.AddTransient<IResource, UserGroupResource>();
            return services;
        }
    }
}