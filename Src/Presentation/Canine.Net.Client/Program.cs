namespace Canine.Net.User.PostgreSQL.Client
{
    using static Canine.Net.Infrastructure.Console.Program;
    using Canine.Net.Infrastructure.Console;
    using Microsoft.Extensions.DependencyInjection;
    using System.Linq;
    using static Monosoft.User.Persistence.PostgreSQL.DependencyInjection;
    using static Monosoft.User.Implementation.DependencyInjection;
    using Canine.Net.Infrastructure.Config;

    public static class Program
    {
        public static void Main()
        {
            var pwd = System.Environment.GetEnvironmentVariable("sqlserver_password");
            var usr = System.Environment.GetEnvironmentVariable("sqlserver_user");
            var databasename = System.Environment.GetEnvironmentVariable("sqlserver_databasename");
            var port = System.Environment.GetEnvironmentVariable("sqlserver_port");
            var servername = System.Environment.GetEnvironmentVariable("sqlserver_name");

            ServiceProvider serviceProvider = new ServiceCollection()
                .AddImplementation()
                .AddPostgreSQLPersistenceProvider(servername, usr, pwd, int.Parse(port), databasename)
                .AddResources()
                .BuildServiceProvider();

            var rabbitMQUsername = System.Environment.GetEnvironmentVariable("rabbitmq_username");
            var rabbitMQPassword = System.Environment.GetEnvironmentVariable("rabbitmq_password");
            var rabbitMQHost = System.Environment.GetEnvironmentVariable("rabbitmq_host");
            var rabbitMQPort = System.Environment.GetEnvironmentVariable("rabbitmq_port");

            RabbitMQSettings rabbitSet = new RabbitMQSettings(rabbitMQUsername, rabbitMQPassword, rabbitMQHost, rabbitMQPort != null ? int.Parse(rabbitMQPort) : 5672);

            StartRabbitMq("User",new ProgramVersion(1), serviceProvider.GetServices<IResource>().ToList(), rabbitSet);
        }
    }
}